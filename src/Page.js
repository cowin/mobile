import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { StatusBar } from 'expo-status-bar'
import { StyleSheet } from 'react-native'
import { Appbar, Modal, Portal, Text, Title } from 'react-native-paper'
import { SafeAreaView } from 'react-native-safe-area-context'
import { connect } from 'react-redux'

import HomeScreen from './components/screens/HomeScreen'

const Header = () => {
	return (
		<Appbar style={styles.appbar}>
			<Title style={styles.appbarTitle}>CoWin</Title>
		</Appbar>
	)
}

const LoadingModal = ({ loadingModal }) => {
	if (loadingModal.show) {
		return (
			<Portal>
				<Modal
					visible={true}
					onDismiss={() => {}}
					contentContainerStyle={styles.loadingModal}
				>
					<Text>Loading ...</Text>
				</Modal>
			</Portal>
		)
	} else return <></>
}

const mapStateToProps = state => ({
	loadingModal: state.loadingModal,
})

const Page = ({ loadingModal }) => {
	return (
		<SafeAreaView style={{ flex: 1 }}>
			<Header />
			<NavigationContainer>
				<Stack.Navigator screenOptions={{ headerShown: false }}>
					<Stack.Screen name='Home' component={HomeScreen}></Stack.Screen>
				</Stack.Navigator>
			</NavigationContainer>
			<LoadingModal loadingModal={loadingModal} />
			<StatusBar style='auto' />
		</SafeAreaView>
	)
}

const Stack = createStackNavigator()

const styles = StyleSheet.create({
	appbar: {
		paddingLeft: 16,
	},
	appbarTitle: {
		color: '#fff',
	},
	loadingModal: {
		alignItems: 'center',
		backgroundColor: '#fff',
		flex: 1,
		justifyContent: 'center',
		padding: 20,
	},
})

export default connect(mapStateToProps)(Page)
