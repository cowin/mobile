import React, { useEffect, useState } from 'react'
import axios from 'axios'
import DropDown from 'react-native-paper-dropdown'
import { ScrollView, StyleSheet, View } from 'react-native'
import {
	Button,
	Chip,
	IconButton,
	Text,
	TextInput,
	useTheme,
} from 'react-native-paper'
import { connect } from 'react-redux'

import {
	loadingModalHide,
	loadingModalShow,
} from '../../actions/loadingModalActions'
import { getData, storeData } from '../../actions/storageActions'

const API_FIND_BY_DISTRICT =
	'https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByDistrict'
const API_FIND_BY_PINCODE =
	'https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByPin'
const API_GET_DISTRICTS =
	'https://cdn-api.co-vin.in/api/v2/admin/location/districts'
const API_GET_STATES = 'https://cdn-api.co-vin.in/api/v2/admin/location/states'
const HEADERS = {
	accept: 'accept: application/json',
	'Accept-Language': 'en_us',
	'user-agent':
		'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
}

const doubleDigit = num => (num < 10 ? '0' + num : num)

const getFullDateString = (date = new Date()) =>
	`${doubleDigit(date.getDate())}-${doubleDigit(
		date.getMonth() + 1
	)}-${date.getFullYear()}`

const getDate = (date = new Date()) => doubleDigit(date.getDate())

const getDayString = (date = new Date()) => {
	switch (date.getDay()) {
		case 0:
			return 'Sun'
		case 1:
			return 'Mon'
		case 2:
			return 'Tue'
		case 3:
			return 'Wed'
		case 4:
			return 'Thu'
		case 5:
			return 'Fri'
		case 6:
			return 'Sat'
		default:
			return ''
	}
}

const getMonthString = (date = new Date(), date2 = new Date()) => {
	const showNextMonth = date.getDate() > date2.getDate()
	switch (date.getMonth()) {
		case 0:
			return showNextMonth ? 'Jan/Feb' : 'Jan'
		case 1:
			return showNextMonth ? 'Feb/Mar' : 'Feb'
		case 2:
			return showNextMonth ? 'Mar/Apr' : 'Mar'
		case 3:
			return showNextMonth ? 'Apr/May' : 'Apr'
		case 4:
			return showNextMonth ? 'May/Jun' : 'May'
		case 5:
			return showNextMonth ? 'Jun/Jul' : 'Jun'
		case 6:
			return showNextMonth ? 'Jul/Aug' : 'Jul'
		case 7:
			return showNextMonth ? 'Aug/Sep' : 'Aug'
		case 8:
			return showNextMonth ? 'Sep/Oct' : 'Sep'
		case 9:
			return showNextMonth ? 'Oct/Nov' : 'Oct'
		case 10:
			return showNextMonth ? 'Nov/Dec' : 'Nov'
		case 11:
			return showNextMonth ? 'Dec/Jan' : 'Dec'
		default:
			return ''
	}
}

const getWeek = (today = new Date()) => {
	const week = []

	for (let index = 0; index < 7; index++) {
		week.push(
			new Date(today.getFullYear(), today.getMonth(), today.getDate() + index)
		)
	}

	return week
}

const HomeScreen = ({ loadingModalHide, loadingModalShow }) => {
	const initialData = {
		district: 0,
		state: 0,
		pincode: '',
	}

	const [districts, setDistricts] = useState([])
	const [data, setData] = useState(initialData)
	const [tableHeaders, setTableHeaders] = useState([])
	const [tableRows, setTableRows] = useState([])
	const [showDistricts, setShowDistricts] = useState(false)
	const [showStates, setShowStates] = useState(false)
	const [states, setStates] = useState([])

	const localClasses = {}

	const changeHandlerDistrict = value => {
		setData({ ...data, district: value })
		loadingModalShow('HomeScreenFindByDistrict')
		findByDistrict(value, new Date())
	}

	const changeHandlerState = value => {
		setData({ ...initialData, state: value })
		setTableHeaders([])
		setTableRows([])

		loadingModalShow('HomeScreenGetDistricts')
		axios
			.get(`${API_GET_DISTRICTS}/${value}`, { headers: HEADERS })
			.then(res => setDistricts(res.data.districts))
			.catch(err => {
				console.log('Failed to fetch districts.')
				console.error(err)
			})
			.finally(() => loadingModalHide('HomeScreenGetDistricts'))
	}

	const changeHandlerPincode = value => {
		setData({ ...initialData, pincode: value })
	}

	const changeWeekCurrent = () => findByDistrict(data.district, new Date())

	const changeWeekNext = () => {
		const day = new Date(tableHeaders[6])
		day.setDate(day.getDate() + 1)

		findByDistrict(data.district, day)
	}

	const changeWeekPrevious = () => {
		const day = new Date(tableHeaders[0])
		day.setDate(day.getDate() - 7)

		if (day < getWeek()[0]) return null

		findByDistrict(data.district, day)
	}

	const compileCenters = resData => {
		const rowsData = resData.map(center => {
			center.total_availability = 0
			center.min_age_limit = 100
			center.sessions.map(session => {
				if (center[session.date]) center[session.date].push(session)
				else center[session.date] = [session]

				if (center.min_age_limit > session.min_age_limit)
					center.min_age_limit = session.min_age_limit

				return (center.total_availability += session.available_capacity)
			})
			delete center.sessions
			return center
		})
		rowsData.sort((a, b) => {
			if (a.total_availability === 0 && a.total_availability === 0) return 0
			else if (a.total_availability === 0) return 1
			else if (b.total_availability === 0) return -1

			if (a.min_age_limit - b.min_age_limit !== 0)
				return a.min_age_limit - b.min_age_limit
			else return b.total_availability - a.total_availability
		})
		setTableRows(rowsData)
	}

	const findByDistrict = (district_id, day) =>
		axios
			.get(
				`${API_FIND_BY_DISTRICT}?district_id=${district_id}&date=${getFullDateString(
					day
				)}`,
				{ headers: HEADERS }
			)
			.then(res => {
				compileCenters(res.data.centers)
				setTableHeaders(getWeek(day))
			})
			.catch(err => {
				console.log('Failed to fetch slots by district.')
				console.error(err)
			})
			.finally(() => loadingModalHide('HomeScreenFindByDistrict'))

	const saveInputs = async () => {
		storeData('data', data)
		storeData('districts', districts)
		storeData('states', states)
	}

	const submitPincodeHandler = () => {
		loadingModalShow('HomeFindByPincode')
		axios
			.get(
				`${API_FIND_BY_PINCODE}?pincode=${
					data.pincode
				}&date=${getFullDateString(new Date())}`,
				{ headers: HEADERS }
			)
			.then(res => {
				compileCenters(res.data.centers)
				setTableHeaders(getWeek())
			})
			.catch(err => {
				console.log('Failed to fetch slots by pincode.')
				console.error(err)
			})
			.finally(() => loadingModalHide('HomeFindByPincode'))
	}

	useEffect(() => {
		getData('states').then(saved_states => {
			if (saved_states) setStates(saved_states)

			if (!saved_states || saved_states.length === 0) {
				loadingModalShow('HomeScreenGetStates')
				axios
					.get(API_GET_STATES, { headers: HEADERS })
					.then(res => setStates(res.data.states))
					.catch(err => {
						console.log('Failed to fetch states.')
						console.error(err)
					})
					.finally(() => loadingModalHide('HomeScreenGetStates'))
			}
		})

		getData('data').then(saved_data => {
			if (saved_data) {
				setData(saved_data)
				if (saved_data.district && saved_data.district !== 0)
					findByDistrict(saved_data.district, new Date())
				else if (saved_data.pincode && saved_data.pincode !== '')
					changeHandlerPincode({ target: { value: saved_data.pincode } })
			}
		})
		getData('districts').then(saved_districts => {
			if (saved_districts) {
				setDistricts(saved_districts)
			}
		})
		// eslint-disable-next-line
	}, [])

	return (
		<View style={localStyles.root}>
			<View className={localClasses.inputs}>
				<DropDown
					inputProps={{
						dense: true,
						right: <TextInput.Icon autoFocus={true} name={'menu-down'} />,
					}}
					label={'States'}
					list={states.map(state => ({
						label: state.state_name,
						value: state.state_id,
					}))}
					mode={'outlined'}
					onDismiss={() => setShowStates(false)}
					setValue={changeHandlerState}
					showDropDown={() => setShowStates(true)}
					value={data.state}
					visible={showStates}
				/>
				<DropDown
					inputProps={{
						dense: true,
						right: <TextInput.Icon name={'menu-down'} />,
					}}
					label={'Districts'}
					list={districts.map(district => ({
						label: district.district_name,
						value: district.district_id,
					}))}
					mode={'outlined'}
					onDismiss={() => setShowDistricts(false)}
					setValue={changeHandlerDistrict}
					showDropDown={() => setShowDistricts(true)}
					value={data.district}
					visible={showDistricts}
				/>
				<Text style={{ fontSize: 18, margin: 3, textAlign: 'center' }}>or</Text>
				<TextInput
					dense
					keyboardType='phone-pad'
					label='Pincode'
					mode='outlined'
					onChangeText={changeHandlerPincode}
					right={<TextInput.Icon onPress={submitPincodeHandler} name='check' />}
					type='number'
					value={data.pincode}
				/>
				<View style={localStyles.dateController}>
					<Button color='#e0e0e0' compact onPress={saveInputs} mode='contained'>
						Save
					</Button>
					<IconButton
						icon='chevron-left'
						size={26}
						style={localStyles.lightColor}
						onPress={changeWeekPrevious}
					/>
					<Text
						style={{ fontSize: 20, marginLeft: 6, ...localClasses.lightColor }}
					>
						{getMonthString(tableHeaders[0], tableHeaders[6])}{' '}
					</Text>
					<IconButton
						icon='chevron-right'
						size={26}
						style={localStyles.lightColor}
						onPress={changeWeekNext}
					/>
					<View>
						<Button
							compact
							labelStyle={{ color: '#000', ...localClasses.lightColor }}
							mode='outlined'
							onPress={changeWeekCurrent}
						>
							This Week
						</Button>
					</View>
				</View>
			</View>
			<View style={{ flex: 1 }}>
				<SessionsTable headers={tableHeaders} rows={tableRows} />
			</View>
		</View>
	)
}

const SessionsTable = ({ headers, rows }) => {
	const { colors } = useTheme()

	if (headers.length > 0)
		return (
			<ScrollView horizontal={true}>
				<View>
					<View style={{ ...localStyles.tableRow, ...localStyles.lightColor }}>
						<View style={localStyles.hospitalCell}>
							<Text style={{ fontSize: 16 }}>Hospital</Text>
						</View>
						{headers.map(header => (
							<View key={header} style={localStyles.tableCell}>
								<Text style={localStyles.tableHeaderText}>
									{getDayString(header)}
								</Text>
								<Text
									style={{
										...localStyles.tableHeaderText,
										...localStyles.tableHeaderDate,
									}}
								>
									{getDate(header)}
								</Text>
							</View>
						))}
					</View>
					<ScrollView horizontal={true}>
						<ScrollView>
							{rows.length === 0 && (
								<View style={{ padding: 12 }}>
									<Text>No Sessions Available</Text>
								</View>
							)}
							{rows.length > 0 &&
								rows.map(row => (
									<View key={row.name} style={localStyles.tableRow}>
										<View style={localStyles.hospitalCell}>
											<Text style={localStyles.hospitalName}>{row.name} </Text>

											{row.fee_type !== 'Free' && (
												<View
													style={{ flexDirection: 'row', marginVertical: 3 }}
												>
													<Chip
														style={{ backgroundColor: colors.primary }}
														textStyle={{ color: '#fff', fontSize: 12 }}
													>
														{row.fee_type}
													</Chip>
												</View>
											)}
											<Text>
												{row.address}, {row.pincode}
											</Text>
										</View>
										{headers.map((header, index) => {
											const sessions = row[getFullDateString(header)]
											if (sessions)
												return (
													<View align='center' key={index}>
														{sessions.map(session => (
															<View
																style={localStyles.tableCell}
																key={session.session_id}
															>
																<Text style={localStyles.tableCellText}>
																	{session.vaccine}
																</Text>
																<Text style={localStyles.tableCellText}>
																	{session.min_age_limit}+
																</Text>
																{session.available_capacity > 0 ? (
																	<Button
																		color={colors.accent}
																		compact
																		mode='contained'
																	>
																		Book ({session.available_capacity})
																	</Button>
																) : (
																	<Button compact disabled mode='contained'>
																		Booked
																	</Button>
																)}
															</View>
														))}
													</View>
												)
											else
												return (
													<View key={index} style={localStyles.tableCell}>
														<Text style={localStyles.tableCellText}>-</Text>
													</View>
												)
										})}
									</View>
								))}
						</ScrollView>
					</ScrollView>
				</View>
			</ScrollView>
		)
	else return <></>
}

const localStyles = StyleSheet.create({
	dateController: {
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center',
		margin: 6,
	},
	hospitalCell: {
		justifyContent: 'center',
		padding: 12,
		width: 140,
	},
	hospitalName: {
		fontSize: 16,
		fontWeight: 'bold',
	},
	lightColor: {
		opacity: 0.5,
	},
	root: {
		flex: 1,
		padding: 12,
		textAlign: 'center',
	},
	tableCell: {
		padding: 12,
		width: 120,
	},
	tableCellText: {
		fontSize: 16,
		textAlign: 'center',
	},
	tableHeaderText: {
		fontSize: 16,
		textAlign: 'center',
	},
	tableHeaderDate: {
		fontSize: 24,
		fontWeight: 'bold',
	},
	tableRow: {
		flexDirection: 'row',
		marginBottom: 12,
	},
})

export default connect(null, { loadingModalHide, loadingModalShow })(HomeScreen)
