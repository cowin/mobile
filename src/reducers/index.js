import { combineReducers } from 'redux'

import loadingModalReducer from './loadingModalReducer'

export default combineReducers({
	loadingModal: loadingModalReducer,
})
