import { LOADING_HIDE, LOADING_SHOW } from './types'

const initialState = {
	ids: [],
	show: false,
}

const loadingModalReducer = (state = initialState, action) => {
	switch (action.type) {
		case LOADING_HIDE:
			if (state.ids.includes(action.payload))
				return {
					ids: state.ids.filter(id => id !== action.payload),
					show: state.ids.length - 1 !== 0,
				}
			return state
		case LOADING_SHOW:
			return {
				ids: [...state.ids, action.payload],
				show: true,
			}
		default:
			return state
	}
}

export default loadingModalReducer
