import 'react-native-gesture-handler'
import React from 'react'
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import { Provider as StoreProvider } from 'react-redux'

import Page from './Page'
import store from './store'

const theme = {
	...DefaultTheme,
	colors: {
		...DefaultTheme.colors,
		primary: '#3f51b5',
		accent: '#f50057',
	},
}

const App = () => (
	<PaperProvider theme={theme}>
		<StoreProvider store={store}>
			<SafeAreaProvider>
				<Page />
			</SafeAreaProvider>
		</StoreProvider>
	</PaperProvider>
)

export default App
